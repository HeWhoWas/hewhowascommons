package com.hewhowas.commons.util;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class FileUtil {

	public static File findFileRecursively(File root, String filename, int depth) {
	    File[] listOfFiles = root.listFiles();
	    for (int i = 0; i < listOfFiles.length; i++) {
	        String iName = listOfFiles[i].getName();
	        if (listOfFiles[i].isFile()) {
	            if (iName.equals(filename)) {
	                return listOfFiles[i];
	            }
	        }
	        else if (listOfFiles[i].isDirectory()) {
	            findFileRecursively(listOfFiles[i], filename, depth+1);
	        }
	    }
	    return null;
	}	
	
	public static String readFile(String pathname) throws IOException {

	    File file = new File(pathname);
	    StringBuilder fileContents = new StringBuilder((int)file.length());
	    Scanner scanner = new Scanner(file);
	    String lineSeparator = System.getProperty("line.separator");

	    try {
	        while(scanner.hasNextLine()) {        
	            fileContents.append(scanner.nextLine() + lineSeparator);
	        }
	        return fileContents.toString();
	    } finally {
	        scanner.close();
	    }
	}
	
	public static void writeStringToFile(String pathname, String data){
		PrintWriter out = null;
		try {
			out = new PrintWriter(pathname);
			out.write(data);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(out != null){
				out.close();
			}
		}
	}

	public static long calculateBytesPerMillisecond(long bytesTransferred, Date startedTime) {
		Date now = new Date();
		long millisecondsTaken = now.getTime() - startedTime.getTime();
		long bytesPerMillisecond = bytesTransferred / millisecondsTaken;
		return bytesPerMillisecond;
	}

    public static String[] getSubfolders(String path){
        return new File(path).list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return new File(dir, name).isDirectory();
            }
        });
    }

    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if(!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        }
        finally {
            if(source != null) {
                source.close();
            }
            if(destination != null) {
                destination.close();
            }
        }
    }
}
