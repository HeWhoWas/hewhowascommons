package com.hewhowas.commons.util;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 3/09/13
 * Time: 10:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class OSUtil {
    public enum OperatingSystem {
      WINDOWS,
      WINDOWS_X64,
      LINUX,
      LINUX_X64,
      OSX,
      OSX_X64
    }

    public static class UnknownOperatingSystemException extends RuntimeException{}

    public static OperatingSystem detectOperatingSystem(){
        String os = System.getProperty("os.name").toLowerCase();

        //Windows
        if(os.contains("windows")){
            //64Bit?
            if(System.getenv("ProgramFiles(x86)") != null)
                return OperatingSystem.WINDOWS_X64;
            else
                return OperatingSystem.WINDOWS;
        } else if(os.contains("nix")){
            return OperatingSystem.LINUX;
        } else if(os.contains("mac")){
            return OperatingSystem.OSX;
        }
        throw new UnknownOperatingSystemException();
    }


}
