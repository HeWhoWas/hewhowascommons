package com.hewhowas.commons.autoupdate;

import java.net.URI;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 9/09/13
 * Time: 5:44 PM
 * Basic interface for AutoUpdate. Provides minimal necessary information.
 */
public interface AutoUpdateResponse {
    public String getUpdateUrl();
    String getNewVersionString();
    int getNewVersionNo();
}
