package com.hewhowas.commons.autoupdate;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 9/09/13
 * Time: 5:30 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AutoUpdateResponseListener {
    void onAutoUpdateResponseSuccess(AutoUpdateResponse response);
    void onAutoUpdateResponseFailure(AutoUpdateException exception);
}
