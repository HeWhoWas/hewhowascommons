package com.hewhowas.commons.autoupdate;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 9/09/13
 * Time: 5:01 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AutoUpdater {
    public void fetchData() throws AutoUpdateException, URISyntaxException;
}
