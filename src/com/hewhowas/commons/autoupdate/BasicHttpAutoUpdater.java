package com.hewhowas.commons.autoupdate;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 9/09/13
 * Time: 4:57 PM
 * Basic HTTP implementation of AutoUpdater that collects data from a remotely hosted java file. Properties format is:
 * availableVersionNo:[int]
 * availableVersionString:[string]
 * downloadUrl:[string]
 */
public class BasicHttpAutoUpdater implements AutoUpdater, AutoUpdateResponse, Runnable {
    protected String updateFileUrl;
    protected HttpClient client;
    protected int versionNo;
    protected String downloadPath;
    protected String versionString;

    public BasicHttpAutoUpdater(String updateFileUrl){
        this.updateFileUrl = updateFileUrl;
    }

    @Override
    public void fetchData() throws AutoUpdateException, URISyntaxException {
        HttpClient client = new DefaultHttpClient();
        HttpUriRequest req = new HttpGet(updateFileUrl);
        try {
            HttpResponse response = client.execute(req);
            try {
                    HttpEntity entity = response.getEntity();
                String body = EntityUtils.toString(entity);
                //Todo: Parse data
                final Properties p = new Properties();
                p.load(new StringReader(body));
                String tmp;
                tmp = p.getProperty("availableVersionNo");
                if(tmp != null && tmp.trim() != ""){
                    versionNo = Integer.parseInt(tmp);
                }
                versionString = p.getProperty("availableVersionString");
                downloadPath = p.getProperty("downloadUrl");
                EntityUtils.consume(entity);
            } catch (IOException e){
                throw new AutoUpdateException(e);
            }
        } catch (IOException e1) {
            throw new AutoUpdateException(e1);
        }
    }

    @Override
    public String getUpdateUrl() {
        return downloadPath;
    }

    @Override
    public String getNewVersionString(){
        return versionString;
    }

    @Override
    public int getNewVersionNo(){
        return versionNo;
    }

    List<AutoUpdateResponseListener> listeners = new ArrayList<AutoUpdateResponseListener>();

    public void addAutoUpdateResponseListener(AutoUpdateResponseListener listener){
        listeners.add(listener);
    }

    public void removeAllAutoUpdateResponseListeners(){
        listeners.clear();
    }

    public void removeAutoUpdateResponseListener(AutoUpdateResponseListener listener){
        listeners.remove(listener);
    }

    protected void notifyAutoUpdateResponseListeners(boolean success, AutoUpdateException exception){
        for(AutoUpdateResponseListener listener : listeners){
            if(success){
                listener.onAutoUpdateResponseSuccess(this);
            } else {
                listener.onAutoUpdateResponseFailure(exception);
            }
        }
    }

    @Override
    public void run() {
        try {
            fetchData();
            notifyAutoUpdateResponseListeners(true, null);
        } catch (AutoUpdateException e) {
            notifyAutoUpdateResponseListeners(false, e);
        } catch (URISyntaxException e) {
            notifyAutoUpdateResponseListeners(false, new AutoUpdateException(e));
        }
    }
}
