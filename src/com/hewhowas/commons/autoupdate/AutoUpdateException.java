package com.hewhowas.commons.autoupdate;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 9/09/13
 * Time: 5:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class AutoUpdateException extends Exception {
    public AutoUpdateException(){
        super();
    }

    public AutoUpdateException(String message){
        super(message);
    }

    public AutoUpdateException(Throwable t){
        super(t);
    }
}
